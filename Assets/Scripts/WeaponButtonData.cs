﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WeaponButtonData : BuyButtonData
{
	public int originalCost;

	void Start ()
	{
        cost = InspectorCost;
        originalCost = Cost;
        UnitShop.OnMoneyChange += HandleOnMoneyChange;
		thisButton = gameObject.GetComponent<Button> ();
	}

    public void SetRefundPrice(int refundAmount)
    {
        Cost = originalCost - refundAmount;
        HandleOnMoneyChange();
    }
}
