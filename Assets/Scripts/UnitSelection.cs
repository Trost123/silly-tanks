﻿using UnityEngine;
using System.Collections;

public class UnitSelection : MonoBehaviour
{

    static GameObject selectedTank;
    static GameObject mainCamera;
    UIManager uiManager;
    // Use this for initialization
    void Start()
    {
        mainCamera = GameObject.Find("Main Camera");
        uiManager = gameObject.GetComponent<UIManager>();
    }

    public void ChangeSelection(GameObject target)
    {
        if (target.name.Contains("Tank"))
        {
            if (target == selectedTank)
            {
                selectedTank.GetComponent<TankLogic>().SwitchCameraMode(true);
                mainCamera.SetActive(false);
                return;
            }
            SelectionToggler(false);
            selectedTank = target;
            //Time.timeScale = 0;
            SelectionToggler(true);
            TankLogic tankLogic = selectedTank.GetComponent<TankLogic>();
            uiManager.ShowBottomBar(tankLogic.IconLevels, tankLogic.WeaponRefundAmount, tankLogic.currentTurretId);
        }
        else if (target.name.Contains("Ground") && selectedTank)
        {
            //Time.timeScale = 1;
            SelectionToggler(false);
            selectedTank = null;
            uiManager.HideBottomBar();
        }
    }

    public void SelectionToggler(bool value)
    {
        if (selectedTank)
        {
            TankLogic anTankScript = selectedTank.GetComponent<TankLogic>();
            anTankScript.ToggleSelect(value);
            if (value == false)
            {
                SwitchToMainCamera(anTankScript);
            }
        }
    }

    public static void SwitchToMainCamera(TankLogic anTankScript = null)
    {
        if (selectedTank)
        {
            if (anTankScript == null)
            {
                anTankScript = selectedTank.GetComponent<TankLogic>();
            }
            anTankScript.SwitchCameraMode(false);
            mainCamera.SetActive(true);
        }
    }

    public void SetTankWeapon(WeaponButtonData weaponButtonData)
    {
        if (selectedTank)
        {
            TankLogic tankScript = selectedTank.GetComponent<TankLogic>();
            if (tankScript.currentTurretId != weaponButtonData.Id)
            {
                UnitShop.Money -= weaponButtonData.Cost;
                tankScript.SetWeapon(weaponButtonData.Id);
                tankScript.WeaponRefundAmount = Mathf.RoundToInt(weaponButtonData.Cost * 0.8f);
                uiManager.ShowBottomBar(selectedTank.GetComponent<TankLogic>().IconLevels, tankScript.WeaponRefundAmount, tankScript.currentTurretId);
            }
        }
    }

    public void SetTankMode(string modeName)
    {
        if (selectedTank)
        {
            TankLogic tankScript = selectedTank.GetComponent<TankLogic>();
            tankScript.SetMode(modeName, false);
        }
    }

    public void SetTankSubmode(string submodeName)
    {
        if (selectedTank)
        {
            TankLogic tankScript = selectedTank.GetComponent<TankLogic>();
            tankScript.SetMode(submodeName, true);
        }
    }

    public void SetTankUpgrade(UpgradeButtonData buttonData)
    {
        TankLogic tankScript = selectedTank.GetComponent<TankLogic>();
        tankScript.SetUpgrade(buttonData.Id);
        UnitShop.Money -= buttonData.Cost;
        buttonData.CurrentLevel++;
        uiManager.ShowBottomBar(selectedTank.GetComponent<TankLogic>().IconLevels);
    }

    public void SwtichTankDuel()
    {
        TankLogic tankScript = selectedTank.GetComponent<TankLogic>();
        tankScript.SwitchDuelmode();
    }

    public void SwtichTankMovement()
    {
        TankLogic tankScript = selectedTank.GetComponent<TankLogic>();
        tankScript.SwitchMovement();
    }
}
