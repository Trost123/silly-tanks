﻿using UnityEngine;
using System.Collections;

public class HpBar : MonoBehaviour
{
	// Use this for initialization
	void Start ()
	{

	}

	public void Hurt (float amount, float maxHp)
	{
		float currentPercentage = 0.4f * amount / maxHp;
		transform.localScale = new Vector3 (currentPercentage, 0.1f, 0.1f);
	}
}
