﻿using UnityEngine;
using System.Collections;

public class Dynamite : MonoBehaviour
{
	public GameObject ExplosionBullet;
	public GameObject ExplosionFx;
	public GameObject Model;

	int lifetime;

	// Update is called once per frame
	void FixedUpdate ()
	{
		lifetime++;
		if (lifetime == 120) {
			ExplosionBullet.SetActive (true);
			ExplosionFx.SetActive (true);
			Model.SetActive (false);
		}
		if (lifetime == 240) {
			Destroy (gameObject);
		}
	}
}
