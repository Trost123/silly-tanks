﻿using UnityEngine;
using System.Collections;

public class GameSpeedControl : MonoBehaviour
{
    public void PauseUnpause()
    {
        if (Time.timeScale >= 1f)
        {
            Time.timeScale = 0.0F;
        }
        else {
            Time.timeScale = 1.0F;
        }
    }

    public void FastNormal()
    {
        if (Time.timeScale == 1f)
        {
            Time.timeScale = 3.0F;
        }
        else {
            Time.timeScale = 1.0F;
        }
    }
}
