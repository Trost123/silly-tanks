using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TankLogic : MonoBehaviour, IPointerClickHandler
{
	public GameObject[] TurretClasses;
	public float RotationForce = 0.05f;
	public float BaseSpeed = 4f;
	[HideInInspector]
	public int[]
		IconLevels = new int[]{0,0,0,0,0,0,3,0,0,0};

	Transform turretTransform;
	float speed;
	Turret turretScript;
	GameObject closestEnemy;
	GameObject chosenEnemy;
    [HideInInspector]
    public int currentTurretId = -1;
    [HideInInspector]
    public int WeaponRefundAmount = 0;
	string currentMode = "Closest";
	string currentSubmode = "All";
    List<string> modeDict = new List<string> {"MostHP", "LeastHP", "Grouped", "Closest", "Leftmost", "Topmost", "Botmost", "Rightmost"};
    List<string> submodeDict = new List<string> { "All", "NotBurning", "Burning", "NotFrozen", "Frozen"};
    bool duelMode = false;
    bool allowMovement = true;
	GameObject tankCamera;
	bool selected = false;
	UnitSelection selectionScript;
	Rigidbody body;
	EnemyFinder enemyFinder;
	int refreshEnemyCD = 60;
	// Use this for initialization
	void Start ()
	{
		speed = BaseSpeed;
		//IconFrames = new int[]{1, 1, 1};
		body = GetComponent<Rigidbody> ();
		enemyFinder = GetComponent<EnemyFinder> ();
		tankCamera = transform.Find ("TankCamera").gameObject;
		selectionScript = GameObject.Find ("UI").GetComponent<UnitSelection> ();
		turretTransform = transform.Find ("TurretPlace");

		chosenEnemy = enemyFinder.FindEnemy (currentMode, currentSubmode);
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
        if (currentTurretId > -1)
        {
            if (duelMode == false)
            {
                refreshEnemyCD--;
            }
            if (refreshEnemyCD < 1)
            {
                refreshEnemyCD = 60;
                SetMode(currentMode, false);
            }
            if (chosenEnemy && chosenEnemy.tag == "Enemy")
            {
                Vector3 targetPoint = chosenEnemy.transform.Find("Target").position;
                if (Vector3.Distance(transform.position, targetPoint) > turretScript.Range)
                {
                    if (allowMovement)
                    {
                        body.AddForce(transform.forward * speed);

                        //body.AddTorque (Quaternion.Slerp (transform.rotation, targetRotation, Time.deltaTime * 2.0f));  
                        Vector3 targetDelta = chosenEnemy.transform.position - transform.position;
                        //get the angle between transform.forward and target delta
                        float angleDiff = Vector3.Angle(transform.forward, targetDelta);
                        // get its cross product, which is the axis of rotation to
                        // get from one vector to the other
                        Vector3 cross = Vector3.Cross(transform.forward, targetDelta);

                        // apply torque along that axis according to the magnitude of the angle.
                        body.AddTorque(cross * angleDiff * RotationForce);
                    }
                    turretTransform.rotation = calculateTurretRotation(chosenEnemy);
                    if (closestEnemy)
                    {
                        targetPoint = closestEnemy.transform.Find("Target").position;
                        if (Vector3.Distance(transform.position, targetPoint) <= turretScript.Range)
                        {
                            turretTransform.rotation = calculateTurretRotation(closestEnemy);
                            if (turretScript.NeedsEnemyReference && turretScript.CurrentEnemyScript == null && chosenEnemy)
                            {
                                turretScript.CurrentEnemyScript = chosenEnemy.GetComponent<EnemyLogic>();
                                //Debug.Log ("Assigning enemy script to turret");
                            }
                            turretScript.Shoot();
                        }
                    }
                }
                else {
                    turretTransform.rotation = calculateTurretRotation(chosenEnemy);
                    if (turretScript.NeedsEnemyReference && turretScript.CurrentEnemyScript == null && chosenEnemy)
                    {
                        turretScript.CurrentEnemyScript = chosenEnemy.GetComponent<EnemyLogic>();
                        //Debug.Log ("Assigning enemy script to turret");
                    }
                    turretScript.Shoot();
                }
            }
            else {
                if (selected && chosenEnemy)
                {
                    chosenEnemy.GetComponent<EnemyLogic>().SelectionSprite.enabled = false;
                }
                chosenEnemy = enemyFinder.FindEnemy(currentMode, currentSubmode);
                closestEnemy = enemyFinder.FindEnemy("Closest", "All");
                if (selected && chosenEnemy)
                {
                    chosenEnemy.GetComponent<EnemyLogic>().SelectionSprite.enabled = true;
                }
            }

            turretScript.ReloadTick();
        }
	}

	Quaternion calculateTurretRotation (GameObject enemyScript)
	{
		Vector3 targetPoint = enemyScript.transform.Find ("Target").position;
		return Quaternion.LookRotation (targetPoint - turretTransform.position, Vector3.up);
	}

	#region IPointerClickHandler implementation

	public void OnPointerClick (PointerEventData eventData)
	{
		selectionScript.ChangeSelection (gameObject);
	}

	#endregion

	public void ToggleSelect (bool value)
	{
		SpriteRenderer [] sprites = GetComponentsInChildren<SpriteRenderer> ();
		sprites [0].enabled = value;
		sprites [1].enabled = value;
		if (chosenEnemy) {
			chosenEnemy.GetComponent<EnemyLogic> ().SelectionSprite.enabled = value;
		}
		selected = value;
	}

	public void SwitchCameraMode (bool value)
	{
		tankCamera.SetActive (value);
	}

	public void SetWeapon (int id)
	{
		if (id != currentTurretId) {
            IconLevels[5] = id;
			GameObject anTurretClass = TurretClasses [id];
			GameObject newTurret = (Instantiate (anTurretClass, turretTransform.position, turretTransform.rotation) as GameObject);
			turretTransform = newTurret.transform;
			if (turretScript) {
				Destroy (turretScript.gameObject);
			}
			turretTransform.parent = transform;
			//turretScript = TurretTransform.GetComponent (weaponName + "Turret") as Turret;
			turretScript = turretTransform.GetComponent<Turret> ();
            turretScript.Setup();
            for (int i = 1; i < 5; i++)
            {
                turretScript.SetUpgrade(i, IconLevels[i]);
            }
			if (turretScript.NeedsEnemyReference && chosenEnemy) {
				turretScript.CurrentEnemyScript = chosenEnemy.GetComponent<EnemyLogic> ();
			}
			currentTurretId = id;
		}
	}

	public void SetMode (string name, bool isSubmode)
	{
		if (true) { // modeName != currentMode)
			if (!isSubmode) {
                IconLevels[6] = modeDict.IndexOf(name);
                currentMode = name;
			} else {
                IconLevels[7] = submodeDict.IndexOf(name);
                currentSubmode = name;
			}
			if (chosenEnemy) {
				chosenEnemy.GetComponent<EnemyLogic> ().SelectionSprite.enabled = false;
			}
			chosenEnemy = enemyFinder.FindEnemy (currentMode, currentSubmode);
			if (currentMode != "Closest") {
				closestEnemy = enemyFinder.FindEnemy ("Closest", "All");
			} else {
				closestEnemy = null;
			}
			if (chosenEnemy == null) {
				//Debug.Log ("No enemy found in this mode");
			} else if (selected) {
				chosenEnemy.GetComponent<EnemyLogic> ().SelectionSprite.enabled = true;
			}
		}
	}

	public void SetUpgrade (int upgradeID)
	{
		if (IconLevels [upgradeID] < 5) {
			IconLevels [upgradeID]++;
			if (upgradeID == 0) {
				speed = BaseSpeed * (1f + 0.4f * (float)IconLevels [upgradeID]);
			} else if (upgradeID > 0) {
                if (turretScript)
                {
                    turretScript.SetUpgrade(upgradeID, IconLevels[upgradeID]);
                }
			}
		}
	}

    public void SwitchDuelmode()
    {
        duelMode = !duelMode;
        IconLevels[8] = duelMode ? 1 : 0;
    }

    public void SwitchMovement()
    {
        allowMovement = !allowMovement;
        IconLevels[9] = allowMovement ? 0 : 1;
    }
}
