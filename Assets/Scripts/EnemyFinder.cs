using UnityEngine;
using System.Collections.Generic;

public class EnemyFinder : MonoBehaviour
{

	public GameObject FindEnemy (string currentMode, string submode)
	{
		List<GameObject> statusFilteredEnemies;
		if (submode == "All") {
			statusFilteredEnemies = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Enemy"));
		} else if (submode == "Burning" || submode == "NotBurning"
			|| submode == "Frozen" || submode == "NotFrozen") {
			statusFilteredEnemies = filterByStatus (GameObject.FindGameObjectsWithTag ("Enemy"), submode);
		} else {
			Debug.LogError ("Wrong enemy subfilter: " + submode);
			return null;
		}

		List<GameObject> viableEnemies = new List<GameObject> ();
		if (currentMode == "Closest") {
			viableEnemies = statusFilteredEnemies;
		} else if (currentMode == "Leftmost") {
			float bestLeft = 100;
			for (int i = 0; i < statusFilteredEnemies.Count; i++) {
				GameObject anEnemy = statusFilteredEnemies [i];
				float left = anEnemy.transform.position.x;
				if (left < bestLeft) {
					viableEnemies.Clear ();
					bestLeft = left;
					viableEnemies.Add (anEnemy);
				} else if (System.Math.Abs (left - bestLeft) < 0.01f) {
					viableEnemies.Add (anEnemy);
				}
			}
		} else if (currentMode == "Rightmost") {
			float bestRight = -100;
			for (int i = 0; i < statusFilteredEnemies.Count; i++) {
				GameObject anEnemy = statusFilteredEnemies [i];
				float right = anEnemy.transform.position.x;
				if (right > bestRight) {
					viableEnemies.Clear ();
					bestRight = right;
					viableEnemies.Add (anEnemy);
				} else if (System.Math.Abs (right - bestRight) < 0.01f) {
					viableEnemies.Add (anEnemy);
				}
			}
		} else if (currentMode == "Topmost") {
			float bestTop = -100;
			for (int i = 0; i < statusFilteredEnemies.Count; i++) {
				GameObject anEnemy = statusFilteredEnemies [i];
				float top = anEnemy.transform.position.z;
				if (top > bestTop) {
					viableEnemies.Clear ();
					bestTop = top;
					viableEnemies.Add (anEnemy);
				} else if (System.Math.Abs (top - bestTop) < 0.01f) {
					viableEnemies.Add (anEnemy);
				}
			}
		} else if (currentMode == "Botmost") {
			float bestBot = 100;
			for (int i = 0; i < statusFilteredEnemies.Count; i++) {
				GameObject anEnemy = statusFilteredEnemies [i];
				float bot = anEnemy.transform.position.z;
				if (bot < bestBot) {
					viableEnemies.Clear ();
					bestBot = bot;
					viableEnemies.Add (anEnemy);
				} else if (System.Math.Abs (bot - bestBot) < 0.01f) {
					viableEnemies.Add (anEnemy);
				}
			}
		} else if (currentMode == "Grouped") { // crowd detection
			int bestGroup = 0;
			for (int i = 0; i < statusFilteredEnemies.Count; i++) {
				EnemyLogic anEnemyBehaviour = statusFilteredEnemies [i].GetComponent<EnemyLogic> ();
				anEnemyBehaviour.CountNeighbours ();
				int group = anEnemyBehaviour.Neighbours;
				if (bestGroup < group) {
					viableEnemies.Clear ();
					viableEnemies.Add (statusFilteredEnemies [i]);
					bestGroup = group;
				} else if (group == bestGroup) {
					viableEnemies.Add (statusFilteredEnemies [i]);
				}
			}
		} else if (currentMode == "MostHP") {
			float mostHP = 0f;
			for (int i = 0; i < statusFilteredEnemies.Count; i++) {
				GameObject anEnemy = statusFilteredEnemies [i];
				EnemyLogic anEnemyScript = anEnemy.GetComponent<EnemyLogic> ();
				float anHp = anEnemyScript.Health;
				if (anHp > mostHP) {
					viableEnemies.Clear ();
					mostHP = anHp;
					viableEnemies.Add (anEnemy);
				} else if (System.Math.Abs (anHp - mostHP) < 0.01f) {
					viableEnemies.Add (anEnemy);
				}
			}
		} else if (currentMode == "LeastHP") {
			float leastHP = 9999f;
			for (int i = 0; i < statusFilteredEnemies.Count; i++) {
				GameObject anEnemy = statusFilteredEnemies [i];
				EnemyLogic anEnemyScript = anEnemy.GetComponent<EnemyLogic> ();
				float anHp = anEnemyScript.Health;
				if (anHp < leastHP) {
					viableEnemies.Clear ();
					leastHP = anHp;
					viableEnemies.Add (anEnemy);
				} else if (System.Math.Abs (anHp - leastHP) < 0.01f) {
					viableEnemies.Add (anEnemy);
				}
			}
		} else {
			Debug.LogError ("Wrong enemy filter: " + currentMode);
			return null;
		}
		if (viableEnemies.Count > 0) {
			return findClosest (viableEnemies);
		}
		return null;
	}
	
	GameObject findClosest (List<GameObject> enemySubArray)
	{
		GameObject chosenEnemy = null;
		float bestDistance = 999;
		for (int i = 0; i < enemySubArray.Count; i++) {
			GameObject anEnemy = enemySubArray [i];
			float anDistance = Vector3.Distance (transform.position, anEnemy.transform.position);
			if (anDistance < bestDistance) {
				bestDistance = anDistance;
				chosenEnemy = anEnemy;
			}
		}
		return chosenEnemy;
	}

	List<GameObject> filterByStatus (GameObject[] allEnemies, string submode)
	{
		List<GameObject> filteredEnemies = new List<GameObject> ();
		int mode = submode.Contains ("Not") ? 1 : 0;
		Debuff.DebuffName filterBuff;
		if (submode.Contains ("Frozen")) {
			filterBuff = Debuff.DebuffName.Freeze;
		} else if (submode.Contains ("Burning")) {
			filterBuff = Debuff.DebuffName.Burning;
		} else {
			filterBuff = Debuff.DebuffName.Freeze;
			Debug.LogError ("Wrong search filter: " + submode);
		}

		if (mode == 1) {
			submode = submode.Substring (3);
		}
		for (int i = 0; i < allEnemies.Length; i++) {
            if (mode == 0 && allEnemies [i].GetComponent<EnemyLogic> ().DebuffList [filterBuff].Duration > 0) {
				filteredEnemies.Add (allEnemies [i]);
			} else if (mode == 1 && allEnemies [i].GetComponent<EnemyLogic> ().DebuffList [filterBuff].Duration < 1) {
				filteredEnemies.Add (allEnemies [i]);
			}
		}
		return filteredEnemies;
	}
}
