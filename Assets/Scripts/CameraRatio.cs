﻿using UnityEngine;
using System.Collections;

public class CameraRatio : MonoBehaviour
{
	// Use this for initialization
	void Start ()
	{
		float targetaspect = 16.0f / 10.0f;
		float windowaspect = (float)Screen.width / (float)Screen.height;
		float scaleheight = windowaspect / targetaspect;

		// if scaled height is less than current height, add letterbox
		if (scaleheight < 0.99f) {  
			float scalewidth = 1.0f / scaleheight;
			Camera myCam = GetComponent<Camera> ();
			myCam.fieldOfView = Mathf.Round ((scalewidth - ((scalewidth - 1f) * 0.24f)) * 600f) / 10f;
		}
	}
}
