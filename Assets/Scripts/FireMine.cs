﻿using UnityEngine;
using System.Collections;

public class FireMine : MineLogic
{

	// Use this for initialization
	void Start ()
	{
		MineDebuff = new Debuff (Debuff.DebuffName.Burning, 20f, 180);
	}
}
