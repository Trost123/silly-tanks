using UnityEngine;
using System.Collections;

public class MineLogic : MonoBehaviour
{
	public GameObject Explosion;
	public Debuff MineDebuff;

	public float Damage = 200f;
	// Use this for initialization
	void Start ()
	{
	
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.name.Contains ("Enemy")) {
			GameObject explosion = Instantiate (Explosion);
			explosion.transform.position = transform.position;
			other.GetComponent<EnemyLogic> ().ApplyDamage (Damage, MineDebuff);
			Destroy (gameObject);
		}
	}
}
