﻿using UnityEngine;
using System.Collections;

public class FadeSprite : MonoBehaviour
{
	int cdMax = 5;
	int cd;
	Material lrMaterial;
	// Use this for initialization
	void Start ()
	{
		lrMaterial = GetComponent<LineRenderer> ().material;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		cd--;
		if (cd < 1) {
			Color matColor = lrMaterial.color;
			matColor.a = matColor.a - 0.05f;
			lrMaterial.color = matColor;
			cd = cdMax;
			if (matColor.a < 0.1f) {
				Destroy (gameObject);
			}
		}
	}
}
