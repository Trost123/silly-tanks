﻿using UnityEngine;
using System.Collections;

public class ElementalLogic : EnemyLogic
{

	override public void ApplyDamage (float amount, Debuff appliedDebuff = null)
	{

		if (appliedDebuff != null) {
			appliedDebuff.Amount *= 2;
		}
		base.ApplyDamage (amount, appliedDebuff);
	}
}
