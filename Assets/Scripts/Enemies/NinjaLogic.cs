﻿using UnityEngine;
using System.Collections;

public class NinjaLogic : EnemyLogic
{
	int invurnCD = 60;
	public GameObject ShieldModel;
	override protected void SpecialUpdate ()
	{
		invurnCD--;
		if (invurnCD == 0) { // become FUCKING INVINCIBLE
			ShieldModel.SetActive (true);
		} else if (invurnCD < -120) { //stop it plz
			ShieldModel.SetActive (false);
			invurnCD = 60;
		}
	}

	override public void ApplyDamage (float amount, Debuff appliedDebuff = null)
	{
		if (invurnCD > 0) {
			base.ApplyDamage (amount, appliedDebuff);
		}
	}
}
