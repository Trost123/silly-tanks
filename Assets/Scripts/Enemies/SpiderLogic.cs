﻿using UnityEngine;
using System.Collections;

public class SpiderLogic : EnemyLogic
{
	public GameObject Egg;
	int spawnTimer = 240;

	override protected void SpecialUpdate ()
	{
		spawnTimer--;
		if (spawnTimer < 0) {
			spawnSpiderling ();
			spawnTimer = 240;
		}
	}

	void spawnSpiderling ()
	{
		GameObject anEgg = Instantiate (Egg);
		Vector3 eggPosition = transform.position;
		eggPosition.z += 0.8f;
		anEgg.transform.position = eggPosition;
//		if (Random.value > 0.5f) {
//			anEgg.transform.Rotate (Vector3.up, -90f);
//		}
		anEgg.transform.parent = transform.parent;
		AnnounceSpawnlings (1);
	}
}
