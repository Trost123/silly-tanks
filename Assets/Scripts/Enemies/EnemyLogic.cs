﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyLogic : MonoBehaviour
{
    public delegate void SimpleDelegate();
    public delegate void IntDelegate(int amount);
    public static event SimpleDelegate OnEnemyDeath;
    public static event IntDelegate SpawnlingBorn;

    [Header("Wave settings")]
    public int SpawnCD = 240;
    public int WaveSize = 10;

    [Header("Unit stats")]
    public float MaxHealth;
    public int Bounty;
    [HideInInspector]
    public SphereCollider CrowdCollider;
    public SpriteRenderer SelectionSprite;
    [HideInInspector]
    public float Health;
    [HideInInspector]
    public int Neighbours;
    public float MoveSpeed = 2f;
    public HpBar MyHpBar;
    [HideInInspector]
    public Dictionary<Debuff.DebuffName, Debuff> DebuffList = new Dictionary<Debuff.DebuffName, Debuff>();
    public ParticleSystem FreezeParticle;
    public ParticleSystem BurnParticle;
    public GameObject LootFxSample;
    public Animator Animator;

    static Color whiteColor = new Color(1f, 1f, 1f);
    static Color redColor = new Color(1f, 0f, 0f);
    static Color orangeColor = new Color(1f, 0.5f, 0f);
    static Color greenColor = new Color(0.5f, 1f, 0.5f);
    static Color blueColor = new Color(0.2f, 0.2f, 1f);
    int burnCD;
    Debuff burnDamageDebuff = new Debuff(Debuff.DebuffName.BurnDamage);
    Rigidbody body;
    GameObject damageTextSample;
    // Use this for initialization
    void OnEnable()
    {
        DebuffList.Add(Debuff.DebuffName.Freeze, new Debuff(Debuff.DebuffName.Freeze, 0f, 0f));
        DebuffList.Add(Debuff.DebuffName.Burning, new Debuff(Debuff.DebuffName.Burning, 0f, 0f));
    }

    void Start()
    {
        //rangeIndicator = transform.Find ("Range Indicator");
        body = GetComponent<Rigidbody>();
        CrowdCollider = GetComponent<SphereCollider>();
        SelectionSprite = GetComponentInChildren<SpriteRenderer>();
        MaxHealth *= 1 + (EnemySpawner.currentWave * 0.1f);
        Bounty *= 1 + Mathf.RoundToInt(EnemySpawner.currentWave * 0.05f);

        Health = MaxHealth;
        CrowdCollider.enabled = false;
        damageTextSample = transform.Find("DamageText").gameObject;
        SpecialStart();
    }

    virtual protected void SpecialStart()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        TickDebuffs();
        float RealSpeed;
        if (DebuffList[Debuff.DebuffName.Freeze].Duration > 0)
        {
            RealSpeed = MoveSpeed / DebuffList[Debuff.DebuffName.Freeze].Amount;
            if (!FreezeParticle.isPlaying)
            {
                FreezeParticle.Play();
            }
        }
        else {
            RealSpeed = MoveSpeed;
            if (FreezeParticle.isPlaying)
            {
                FreezeParticle.Stop();
            }
        }
        body.velocity = (transform.forward * RealSpeed);

        if (DebuffList[Debuff.DebuffName.Burning].Duration > 0)
        {

            if (!BurnParticle.isPlaying)
            {
                BurnParticle.Play();
            }
            burnCD--;
            if (burnCD < 1)
            {
                ApplyDamage(DebuffList[Debuff.DebuffName.Burning].Amount, burnDamageDebuff);
                burnCD = 20;
            }
        }else {
            if (BurnParticle.isPlaying)
            {
                BurnParticle.Stop();
            }
        }

        if (transform.position.z < -9f)
        {
            // teleport to top
            // transform.position = new Vector3 (transform.position.x, transform.position.y, 5.30f);
            ApplyDamage(9999);
        }

        SpecialUpdate();
    }

    virtual protected void SpecialUpdate()
    {

    }

    public void CountNeighbours()
    {
        CrowdCollider.enabled = true;
        GameObject[] allEnemies = GameObject.FindGameObjectsWithTag("Enemy");
        Neighbours = 0;
        for (int i = 0; i < allEnemies.Length; i++)
        {
            if (CrowdCollider.bounds.Contains(allEnemies[i].transform.position))
            {
                Neighbours++;
            }
        }
        CrowdCollider.enabled = false;
    }

    void TickDebuffs()
    {
        foreach (KeyValuePair<Debuff.DebuffName, Debuff> entry in DebuffList)
        {
            entry.Value.Duration--;
        }
    }

    virtual public void ApplyDamage(float amount, Debuff appliedDebuff = null)
    {
        if (Health > 1)
        {
            bool isCrit = false;
            Color selectedColor = whiteColor;
            if (appliedDebuff != null)
            {
                if (appliedDebuff.Name == Debuff.DebuffName.Crit)
                {
                    if (Random.value < appliedDebuff.Duration)
                    {
                        amount *= appliedDebuff.Amount;
                        isCrit = true;
                    }
                }
                else if (appliedDebuff.Name == Debuff.DebuffName.Freeze)
                {
                    selectedColor = blueColor;
                    DebuffList[appliedDebuff.Name] = new Debuff(appliedDebuff.Name, appliedDebuff.Amount, appliedDebuff.Duration);
                }
                else if (appliedDebuff.Name == Debuff.DebuffName.Burning)
                {
                    DebuffList[appliedDebuff.Name] = new Debuff(appliedDebuff.Name, appliedDebuff.Amount, appliedDebuff.Duration);
                    burnCD = 60;
                }
                else if (appliedDebuff.Name == Debuff.DebuffName.CritOnFrozen)
                {
                    if (DebuffList[Debuff.DebuffName.Freeze].Duration > 0)
                    {
                        amount *= appliedDebuff.Amount;
                        isCrit = true;
                    }
                }
                else if (appliedDebuff.Name == Debuff.DebuffName.CritOnBurning)
                {
                    if (DebuffList[Debuff.DebuffName.Burning].Duration > 0)
                    {
                        amount *= appliedDebuff.Amount;
                        isCrit = true;
                    }
                }
                else if (appliedDebuff.Name == Debuff.DebuffName.Heal)
                {
                    selectedColor = greenColor;
                }
            }
            if (isCrit)
            {
                selectedColor = redColor;
            }
            else if (appliedDebuff != null && appliedDebuff.Name == Debuff.DebuffName.BurnDamage)
            {
                selectedColor = orangeColor;
            }


            Health -= amount;
            MyHpBar.Hurt(Health, MaxHealth);
            GameObject dmgText = Instantiate(damageTextSample, damageTextSample.transform.position, damageTextSample.transform.rotation) as GameObject;
            TextMesh textMesh = dmgText.GetComponent<TextMesh>();
            textMesh.text = isCrit ? amount + "!" : amount.ToString();
            textMesh.color = selectedColor;
            dmgText.SetActive(true);
        }
        if (Health < 1 && gameObject.tag == "Enemy")
        {
            OnDeathSpecial();
            OnEnemyDeath();
            GameObject anLootFx = Instantiate(LootFxSample, transform.position, Quaternion.identity) as GameObject;
            anLootFx.GetComponent<LootFx>().SetUp(this.Bounty);
            anLootFx.transform.parent = this.transform;
            UnitShop.Money += Bounty;
            GetComponent<BoxCollider>().enabled = false;
            MoveSpeed = 0;
            gameObject.tag = "Untagged";
            MyHpBar.gameObject.SetActive(false);
            Animator.SetBool("isDead", true);
            Destroy(gameObject, 2f);
        }
    }

    virtual protected void OnDeathSpecial()
    {
    }

    protected void AnnounceSpawnlings(int amount)
    {
        SpawnlingBorn(amount);
    }

}
