﻿using UnityEngine;
using System.Collections;

public class SpawnSpiderling : MonoBehaviour
{
	public GameObject Spiderling;
	// Use this for initialization
	void Spawn ()
	{
		GameObject spiderling = Instantiate (Spiderling, transform.position, transform.rotation) as GameObject;
		spiderling.transform.parent = transform.parent;
	}

	void DestroySelf ()
	{
		Destroy (gameObject);
	}
}
