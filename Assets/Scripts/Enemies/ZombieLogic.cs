﻿using UnityEngine;
using System.Collections;

public class ZombieLogic : EnemyLogic
{
	public int regenValue = 20;
	int regenCD = 30;
	override protected void SpecialUpdate ()
	{
		if (Health < MaxHealth - regenValue) {
			if (regenCD < 1) {
				regenCD = 30;
				ApplyDamage (-regenValue, new Debuff (Debuff.DebuffName.Heal));
			}
			regenCD--;
		}
	}
}
