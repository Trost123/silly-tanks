﻿using UnityEngine;
using System.Collections;

public class SlimeLogic : EnemyLogic
{
	public GameObject Baby;
	public GameObject ExplosionFX;
	private Vector3[] childSpawnPoints;
	// Use this for initialization
	override protected void SpecialStart ()
	{
		childSpawnPoints = new Vector3[4];
		childSpawnPoints [0] = new Vector3 (-0.22f, 0f, 0.59f);
		childSpawnPoints [1] = new Vector3 (0.26f, 0f, 0.58f);
		childSpawnPoints [2] = new Vector3 (-0.24f, 0f, -0.48f);
		childSpawnPoints [3] = new Vector3 (0.2f, 0f, -0.54f);
	}

	override protected void OnDeathSpecial ()
	{
		GameObject anFX = Instantiate (ExplosionFX);
		anFX.transform.position = transform.position;
		for (int i = 0; i < 4; i++) {
			GameObject anBaby = Instantiate (Baby);
			anBaby.transform.position = transform.position + childSpawnPoints [i];
			anBaby.transform.parent = transform.parent;
		}
		AnnounceSpawnlings (4);
	}
}
