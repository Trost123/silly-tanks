﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class SummonTooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public string Title;
    public BuyButtonData buyButtonScript;
    [TextArea(1, 5)]
    public string
        Info;
    private string
        infoWithCosts;

    #region IPointerEnterHandler implementation
    public void OnPointerEnter(PointerEventData eventData)
    {
        Vector3 position = gameObject.transform.position;
        position.x += 30;
        position.y += 30;

        if (infoWithCosts != null)
        {
            TooltipLogic.Instance.Show(Title, infoWithCosts, position);
        }else
        {
            TooltipLogic.Instance.Show(Title, Info, position);
        }
    }
    #endregion

    #region IPointerExitHandler implementation

    public void OnPointerExit(PointerEventData eventData)
    {
        TooltipLogic.Instance.Hide();
    }

    #endregion

    // Use this for initialization
    void Awake()
    {
        if (buyButtonScript)
        {
            buyButtonScript.OnCostChange += RefreshTooltipText;
        }
    }

    public void RefreshTooltipText()
    {
        if (buyButtonScript != null)
        {
            if(buyButtonScript is WeaponButtonData && (buyButtonScript as WeaponButtonData).originalCost != buyButtonScript.Cost)
            {
                infoWithCosts = "Change for $" + buyButtonScript.Cost + "\n" + Info;
            }
            else
            {
                infoWithCosts = "Price: $" + buyButtonScript.Cost + "\n" + Info;
            }
            if (TooltipLogic.IsVisible)
            {
                TooltipLogic.Instance.UpdateInfo(infoWithCosts);
            }
        }
        else
        {
            Debug.Log(Info);
            TooltipLogic.Instance.UpdateInfo(Info);
        }
    }
}
