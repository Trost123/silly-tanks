﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonChanger : MonoBehaviour
{
	public Sprite[] SpriteList;
	Image currentImage;
    int currentID = 0;
	// Use this for initialization
	void Start ()
	{
		currentImage = gameObject.GetComponent<Image> ();
	}

	public void SetState (int value)
	{
        if(value == -1)
        {
            currentID++;
            if(currentID > SpriteList.Length-1)
            {
                currentID = 0;
            }
        }
        else
        {
            currentID = value;
        }
		currentImage.sprite = SpriteList [currentID];
	}
}
