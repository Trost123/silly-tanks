﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public RectTransform BottomBar;
    public Animator[] Popups;
    public ButtonChanger[] BottomIcons;
    public ButtonChanger SpecialIcon;
    public Button SpecialButton;
    public SummonTooltip SpecialTooltip;
    public WeaponButtonData[] WeaponButtons;
    private string[] specialTooltips = { "No special for this weapon", "Shotgun will shoot more bullets!","No special too",
        "Fire will burn longer", "Freeze will slow more", "Bigger exlosion area","Higher chanse of critical damage", "More lighning jumps",
        "More damage to poisoned targets", "More damage to slowed targets"};

    // Use this for initialization
    void Start()
    {
        HideBottomBar();
    }

    public void ShowBottomBar(int[] buttonLevels, int refundAmount = -1, int turretID = -2)
    {
        BottomBar.anchoredPosition = new Vector2(BottomBar.anchoredPosition.x, 0);
        for (int i = 0; i < BottomIcons.Length; i++)
        {
            BottomIcons[i].SetState(buttonLevels[i]);
        }
        if (turretID > -2)
        {
            if (turretID != 0 && turretID != 2 && turretID != -1)
            {
                SpecialIcon.SetState(1);
                if (buttonLevels[4] < 5)
                {
                    SpecialButton.interactable = true;
                }
                SpecialTooltip.Info = specialTooltips[turretID];
                SpecialTooltip.RefreshTooltipText();
            }
            else if(turretID != -1)
            {
                SpecialIcon.SetState(0);
                SpecialButton.interactable = false;
                SpecialTooltip.Info = specialTooltips[0];
                SpecialTooltip.RefreshTooltipText();
            }
            else
            {
                SpecialIcon.SetState(0);
                SpecialButton.interactable = false;
                SpecialTooltip.Info = "Buy a weapon first";
                SpecialTooltip.RefreshTooltipText();
            }
        }

        if (refundAmount != -1)
        {
            for (int i = 0; i < WeaponButtons.Length; i++)
            {
                WeaponButtons[i].SetRefundPrice(refundAmount);
            }
        }
    }

    public void HideBottomBar()
    {
        BottomBar.anchoredPosition = new Vector2(BottomBar.anchoredPosition.x, -100);
    }

    public void SwitchPopup(Animator selectedAnimator)
    {
        selectedAnimator.SetBool("ShowPopup", !selectedAnimator.GetBool("ShowPopup"));
        hideOtherSubmenus(selectedAnimator);
    }

    void hideOtherSubmenus(Animator immunne)
    {
        for (int i = 0; i < Popups.Length; i++)
        {
            if (Popups[i] != immunne && Popups[i].GetBool("ShowPopup"))
            {
                Popups[i].SetBool("ShowPopup", !Popups[i].GetBool("ShowPopup"));
            }
        }
    }
}
