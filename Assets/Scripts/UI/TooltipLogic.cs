﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TooltipLogic : MonoBehaviour
{

	//public CanvasGroup tooltip;
	Text titleTextfield;
	Text infoTextfield;

    public static bool IsVisible = false;
	// Standard Singleton Access
	static TooltipLogic instance;
	public static TooltipLogic Instance {
		get {
			return instance;
		}
	}

	void Start ()
	{
		instance = this;
		titleTextfield = GetComponentInChildren<Text> ();
		infoTextfield = GetComponentsInChildren<Text> () [1];
		Hide ();
	}

	public void Show (string title, string info, Vector3 pos)
	{
        IsVisible = true;
        if (titleTextfield.text != title)
			titleTextfield.text = title;
		if (infoTextfield.text != info)
			infoTextfield.text = info;

		Vector3[] corners = new Vector3[4];
		(transform as RectTransform).GetWorldCorners (corners);
		var width = corners [2].x - corners [0].x;
		var height = corners [1].y - corners [0].y;
		
		var distPastX = pos.x + width - Screen.width;
		if (distPastX > 0)
                pos = new Vector3(pos.x - width - 60, pos.y, pos.z);
		var distPastY = pos.y - height;
		if (distPastY < 0)
			pos = new Vector3 (pos.x, pos.y - distPastY, pos.z);
		
		transform.position = pos;

		transform.position = pos;
		gameObject.SetActive (true);
	}

    public void UpdateInfo(string info)
    {
        if (infoTextfield.text != info)
            infoTextfield.text = info;
    }

	public void Hide ()
	{
        IsVisible = false;
        gameObject.SetActive (false);
	}
}
