﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WavePanel : MonoBehaviour
{
    public Sprite[] EnemySprites;
    public UnityEngine.UI.Text WaveTF;
    public UnityEngine.UI.Button StartButton;

    Image[] iconHolders = new Image[4];
    SummonTooltip[] tooltips = new SummonTooltip[4];
    string[] enemyNames = {
        "Alien invader",
        "Drone swarm",
        "Slime",
        "Ufo",
        "Brute",
        "Zombie",
        "Golem",
        "Mr. Skeletal",
        "Spider",
        "Ninja enemy",
        "Noting",
    };
    string[] enemyDescriptions = {
        "Simple enemy.",
        "Spawn in groups.",
        "Splits into small slimes on death.",
        "Fast enemy.",
        "Lots of hp",
        "Regenerates health",
        "Resistant to physical damage, weak to debuffs (fire, slow, laser or chaingun combo)",
        "Immune to fire and frost",
        "Leaves eggs behind it.",
        "Ignores attacks periodically",
        "There are no more waves!"
    };

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < 4; i++)
        {
            Transform anWaveIcon = transform.Find("Enemy icon " + (i + 1));
            iconHolders[i] = anWaveIcon.GetComponent<Image>();
            tooltips[i] = anWaveIcon.GetComponent<SummonTooltip>();
        }
    }

    public void SetUp(int[] waves, int waveNumber)
    {
        for (int i = 0; i < 4; i++)
        {
            iconHolders[i].sprite = EnemySprites[waves[i]];
            tooltips[i].Title = enemyNames[waves[i]];
            tooltips[i].Info = enemyDescriptions[waves[i]];
        }
        WaveTF.text = "Lvl " + (waveNumber + 1) + "/100";
        if (waves[0] != 10)
        {
            StartButton.interactable = true;
        }
    }
}
