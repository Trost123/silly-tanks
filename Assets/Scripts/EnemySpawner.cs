﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour
{
    public GameObject[] EnemyTypes;
    public WavePanel WavePanel;
    private int SpawnCDMax;
    private Transform enemySpawner;
    bool allowSpawn = false;
    int spawnCD = 0;
    [HideInInspector]
    public static int currentWave = 0;
    int enemiesSpawned = 0;
    int enemiesAlive = 10;
    int squadSize = 1;
    int waveSize = 10;
    int[] waves = {0,1,2,3,4,5,6,7,8,9,
        0,1,2,3,4,5,6,7,8,9,0};

    public void StartNextWave()
    {
        allowSpawn = true;
    }

    // Use this for initialization
    void Start()
    {
        enemySpawner = transform.Find("EnemySpawner");
        SetUpPanel();
        EnemyLogic.OnEnemyDeath += CountDeadEnemies;
        EnemyLogic.SpawnlingBorn += IncreaseEnemyCount;

        EnemyLogic nextWaveLogic = (EnemyTypes[waves[currentWave]] as GameObject).GetComponent<EnemyLogic>();
        SpawnCDMax = nextWaveLogic.SpawnCD;
        waveSize = nextWaveLogic.WaveSize;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //WavePanel.SetUp ();
        if (allowSpawn)
        {
            spawnCD--;
            if (spawnCD < 1 && enemiesSpawned < waveSize)
            {
                for (int i = 0; i < squadSize; i++)
                {
                    enemiesSpawned++;
                    MoveToRandomPosition();
                    GameObject anEnemy = Instantiate(EnemyTypes[waves[currentWave]]);
                    anEnemy.transform.parent = transform;
                    anEnemy.transform.position = enemySpawner.position;
                }
                spawnCD = SpawnCDMax;
            }
            else if (allowSpawn && enemiesSpawned == waveSize)
            {
                allowSpawn = false;
            }
        }
    }

    void CountDeadEnemies()
    {
        enemiesAlive--;
        if (enemiesAlive == 0)
        {
            spawnCD = 0;
            currentWave++;
            enemiesSpawned = 0;

            if (currentWave < waves.Length)
            {
                EnemyLogic nextWaveLogic = (EnemyTypes[waves[currentWave]] as GameObject).GetComponent<EnemyLogic>();
                SpawnCDMax = nextWaveLogic.SpawnCD;
                waveSize = nextWaveLogic.WaveSize;
                enemiesAlive = waveSize;
            }
            
            SetUpPanel();
        }
    }

    void IncreaseEnemyCount(int amount)
    {
        enemiesAlive += amount;
    }

    void SetUpPanel()
    {
        int[] next4 = new int[4];
        for (int i = 0; i < 4; i++)
        {
            if(currentWave + i < waves.Length) {
            next4[i] = waves[currentWave + i];
            }else
            {
                next4[i] = 10;
            }
        }
        WavePanel.SetUp(next4, currentWave);
    }

    void MoveToRandomPosition()
    {
        Vector3 randomSpawnPosition = enemySpawner.position;
        randomSpawnPosition.x = Random.Range(-12f, 12f);
        enemySpawner.position = randomSpawnPosition;
    }
}
