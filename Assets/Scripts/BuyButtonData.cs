﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BuyButtonData : MonoBehaviour
{
    public delegate void SimpleDelegate();
    public event SimpleDelegate OnCostChange;

    public int Id;
    public int InspectorCost;
    protected int cost;
    protected Button thisButton;

    void Start ()
	{
        Cost = InspectorCost;
		UnitShop.OnMoneyChange += HandleOnMoneyChange;
		thisButton = gameObject.GetComponent<Button> ();
	}

    public int Cost
    {
        get
        {
            return cost;
        }

        set
        {
            cost = value;
            if (OnCostChange != null)
            {
                OnCostChange();
            }
        }
    }

    protected void HandleOnMoneyChange ()
	{
		thisButton.interactable = UnitShop.Money >= Cost;
	}

    public void DisablePurchases()
    {
        thisButton.interactable = false;
        UnitShop.OnMoneyChange -= HandleOnMoneyChange;
    }
}
