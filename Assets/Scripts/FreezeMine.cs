﻿using UnityEngine;
using System.Collections;

public class FreezeMine : MineLogic
{

	// Use this for initialization
	void Start ()
	{
		MineDebuff = new Debuff (Debuff.DebuffName.Freeze, 0.6f, 240);
	}
}
