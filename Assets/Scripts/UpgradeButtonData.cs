﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UpgradeButtonData : BuyButtonData
{
    public int[] CostPerLevel;
    private int currentLevel = 0;

    public int CurrentLevel
    {
        get
        {
            return currentLevel;
        }

        set
        {
            currentLevel = value;
            if (currentLevel < 5)
            {
                Cost = CostPerLevel[currentLevel];
                HandleOnMoneyChange();
            }
            else
            {
                Cost = 0;
                DisablePurchases();
            }
        }
    }

    void Start ()
	{
        Cost = CostPerLevel[CurrentLevel];
        UnitShop.OnMoneyChange += HandleOnMoneyChange;
		thisButton = gameObject.GetComponent<Button> ();
    }
}
