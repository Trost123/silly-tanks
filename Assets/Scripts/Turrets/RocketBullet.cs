using UnityEngine;
using System.Collections;

public class RocketBullet : BulletBehaviour
{
	public GameObject Explosion;
	
	override protected void OnTriggerEnter (Collider other)
	{
		if (other.name.Contains ("Enemy")) {
			
			//other.GetComponent<EnemyLogic> ().ApplyDamage (Damage, BulletDebuff);
			GameObject anExplosion = Instantiate (Explosion, transform.position, transform.rotation) as GameObject;
			BulletBehaviour explosionScript = anExplosion.GetComponent<BulletBehaviour> ();
			explosionScript.Damage = Damage;
			Destroy (gameObject);
		}
	}
}