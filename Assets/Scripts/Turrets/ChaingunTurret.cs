﻿using UnityEngine;
using System.Collections;

public class ChaingunTurret: Turret
{
	Transform[] bulletSpawnerArray = new Transform[6];
	int spawnerId;
	// Use this for initialization
	void Start ()
	{
		bulletSpawnerArray [0] = BulletSpawner;
		for (int i = 1; i < 6; i++) {
			bulletSpawnerArray [i] = transform.Find ("BulletSpawner_" + i);
		}
	}

	override public void Shoot ()
	{
		if (CurrentReload < 1) {
			spawnerId++;
			if (spawnerId > 5) {
				spawnerId = 0;
			}
			BulletSpawner = bulletSpawnerArray [spawnerId];
		}
		base.Shoot ();
	}

}
