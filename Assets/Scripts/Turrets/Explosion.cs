using UnityEngine;
using System.Collections;

public class Explosion : BulletBehaviour
{
	int lifetime;
	override protected void FixedUpdate ()
	{
		lifetime++;
		if (lifetime == 10) {
			Destroy (gameObject);
		}
	}
}