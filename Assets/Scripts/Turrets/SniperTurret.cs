using UnityEngine;
using System.Collections;

public class SniperTurret : Turret
{
	GameObject sampleRay;
	// Use this for initialization
	void Start ()
	{
		sampleRay = transform.Find ("Ray").gameObject;
		sampleRay.SetActive (false);
	}

	override public void Shoot ()
	{
		if (CurrentReload < 1 && BulletSpawner) {
			//shooot

			GameObject newRay = Instantiate (sampleRay, sampleRay.transform.position, sampleRay.transform.rotation) as GameObject;
			newRay.SetActive (true);
			float lineLength = Vector3.Distance (transform.position, CurrentEnemyScript.transform.position);
			newRay.GetComponent<LineRenderer> ().SetPosition (1, new Vector3 (0f, -0.2f, lineLength));

			CurrentEnemyScript.ApplyDamage (Damage, new Debuff (Debuff.DebuffName.CritOnFrozen, 1.5f));
			CurrentReload = MaxReload;
		}
	}
}
