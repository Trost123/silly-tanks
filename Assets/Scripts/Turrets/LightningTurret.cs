﻿using UnityEngine;
using System.Collections;

public class LightningTurret : Turret
{
	//void Awake ()
	//{
	//	NeedsEnemyReference = true;
	//}
	override public void Shoot ()
	{
		if (CurrentReload < 1 && BulletSpawner) {
			//shooot
			LightningBolt anLightningBolt = (Instantiate (Bullet, BulletSpawner.position, Quaternion.LookRotation (transform.forward)) as GameObject).GetComponent<LightningBolt> ();
			anLightningBolt.Damage = Damage;
			float lineLength = Vector3.Distance (transform.position, CurrentEnemyScript.transform.position);
			anLightningBolt.SetFirstEnemy (CurrentEnemyScript, lineLength, (int)SpecialAmount);
			CurrentReload = MaxReload;
		}
	}
}
