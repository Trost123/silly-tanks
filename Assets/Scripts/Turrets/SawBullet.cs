﻿using UnityEngine;
using System.Collections;

public class SawBullet : BulletBehaviour
{
	Transform spriteTransform;

	void Start ()
	{
		spriteTransform = transform.Find ("SawSprite");
	}

	override protected void FixedUpdate ()
	{
		spriteTransform.Rotate (Vector3.forward, -20f);
		if (Damage > 0f) {
			transform.position += transform.forward * Time.deltaTime * MoveSpeed;
			MoveSpeed += 0.25f;
		}
	}
}
