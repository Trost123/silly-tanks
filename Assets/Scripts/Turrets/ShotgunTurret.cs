using UnityEngine;
using System.Collections;

public class ShotgunTurret : Turret
{

	override public void Shoot ()
	{
		if (CurrentReload < 1 && BulletSpawner) {
			//shooot
			for (int i = 0; i < SpecialAmount; i++) {
				float yAngle = -20 + (40 / (SpecialAmount - 1) * i);
				Quaternion startAngle = Quaternion.LookRotation (transform.forward) * Quaternion.Euler (0, yAngle, 0);
				BulletBehaviour anBullet = (Instantiate (Bullet, BulletSpawner.position, startAngle) as GameObject).GetComponent<BulletBehaviour> ();
				anBullet.Damage = Damage;
			}
			CurrentReload = MaxReload;
		}
	}
}
