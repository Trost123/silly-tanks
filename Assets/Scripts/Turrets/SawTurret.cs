﻿using UnityEngine;
using System.Collections;

public class SawTurret : Turret
{

    override protected void setSpecial(int level)
    {
        SpecialDuration = SpecialUpgrades[level];
    }
}
