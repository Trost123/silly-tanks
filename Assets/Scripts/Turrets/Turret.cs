using UnityEngine;
using System.Collections;

public class Turret : MonoBehaviour
{
	public GameObject Bullet;
	public float BaseDamage = 50;
	public int BaseReload = 20;
	public float BaseRange = 3;
	public Debuff.DebuffName DebuffType;
	public float SpecialAmount = 0f;
	public float SpecialDuration = 0f;
    public float[] SpecialUpgrades;
	public bool NeedsEnemyReference = false;
	public EnemyLogic CurrentEnemyScript;

	//[System.NonSerialized]
	[HideInInspector]
	public float
		Range;
	
	protected Transform BulletSpawner;
	protected int CurrentReload = 0;
	protected float Damage;
	protected int MaxReload;

	Transform rangeIndicator;

	public void Setup ()
	{
		Damage = BaseDamage;
		MaxReload = BaseReload;
		Range = BaseRange;
		rangeIndicator = transform.parent.Find ("Range Indicator");
		BulletSpawner = transform.Find ("BulletSpawner");
		CurrentReload = MaxReload;
		rangeIndicator.localScale = new Vector3 (Range * 10, Range * 10, Range * 10);
	}

	public virtual void ReloadTick ()
	{
		CurrentReload --;
	}

	public virtual void Shoot ()
	{
		if (CurrentReload < 1 && BulletSpawner) {
			//shooot
			BulletBehaviour anBullet = (Instantiate (Bullet, BulletSpawner.position, Quaternion.LookRotation (transform.forward)) as GameObject).GetComponent<BulletBehaviour> ();
			anBullet.Damage = Damage;
			anBullet.transform.parent = GameObject.Find ("BulletContainer").transform;
			if (DebuffType != Debuff.DebuffName.None) {
				anBullet.BulletDebuff = new Debuff (DebuffType, SpecialAmount, SpecialDuration);
			}
			CurrentReload = MaxReload;
		}
	}

	public void SetUpgrade (int id, int level)
	{
		if (id == 1) {
			Range = BaseRange * (1f + 0.4f * (float)level);
			rangeIndicator.localScale = new Vector3 (Range * 10, Range * 10, Range * 10);
		} else if (id == 2) {
			MaxReload = Mathf.RoundToInt (BaseReload / (1f + 0.4f * (float)level));
		} else if (id == 3) {
			Damage = BaseDamage * (1f + 0.4f * (float)level);
		} else if (id == 4)
        {
            setSpecial(level);
        }
	}

    virtual protected void setSpecial(int level)
    {

    }
}
