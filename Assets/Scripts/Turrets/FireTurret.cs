using UnityEngine;
using System.Collections;

public class FireTurret : Turret
{
	// Use this for initialization
	ParticleSystem ps;
	int fireTimer = -1;
	void Start ()
	{
		ps = BulletSpawner.GetComponent<ParticleSystem> ();
	}

	override public void Shoot ()
	{
		if (CurrentReload < 1 && BulletSpawner) {
			//shooot
			if (ps.isStopped) {
				ps.Play ();
				CurrentReload = 40;
				//Invoke()
			} else {
				base.Shoot ();
			}
			fireTimer = 120;
		}
	}
	override public void ReloadTick ()
	{
		CurrentReload --;
		fireTimer--;
		if (fireTimer == 0) {
			fireTimer = -1;
			ps.Stop ();
		}
	}
}
