using UnityEngine;
using System.Collections;

public class LaserTurret : Turret
{
	LineRenderer laserLine;
	// Use this for initialization
	void Start ()
	{
		laserLine = BulletSpawner.GetComponent<LineRenderer> ();
		laserLine.enabled = false;
	}

	override public void Shoot ()
	{
		if (CurrentReload < 1 && BulletSpawner) {
			//shooot
			laserLine.enabled = true;
			float lineLength = Vector3.Distance (transform.position, CurrentEnemyScript.transform.position);
			laserLine.SetPosition (1, new Vector3 (0f, -0.2f, lineLength));
			CurrentEnemyScript.ApplyDamage (Damage, new Debuff (DebuffType, SpecialAmount));
			CurrentReload = MaxReload;
			Invoke ("hideLaser", 0.1f);
		}
	}

	void hideLaser ()
	{
		laserLine.enabled = false;
	}
}
