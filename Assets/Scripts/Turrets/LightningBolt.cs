﻿using UnityEngine;
using System.Collections;

public class LightningBolt : MonoBehaviour
{

	public float Damage = 5f;
	public GameObject BoltFx;
	int searchCD = 20;
	int jumpCount = 1;
	int maxJumps;
	GameObject lastEnemy;
	Vector3 lastPosition;
	// Use this for initialization
	void Start ()
	{
	}

	public void SetFirstEnemy (EnemyLogic anEnemy, float distance, int maxJumps)
	{
		this.maxJumps = maxJumps;
		Vector3 anFxScale = BoltFx.transform.localScale;
		anFxScale.y = distance / 5.15f;
		BoltFx.transform.localScale = anFxScale;

		lastPosition = transform.position;
		transform.position = anEnemy.transform.position;
		transform.rotation = Quaternion.LookRotation (lastPosition - transform.position);
		lastEnemy = anEnemy.gameObject;
		anEnemy.ApplyDamage (Damage);
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		searchCD--;
		if (searchCD < 1) {
			if (jumpCount == maxJumps) {
				Destroy (gameObject);
				return;
			}
			GameObject[] allEnemies = GameObject.FindGameObjectsWithTag ("Enemy");
			GameObject chosenEnemy = null;
			float bestDistance = 999;

			for (int i = 0; i < allEnemies.Length; i++) {
				GameObject anEnemy = allEnemies [i];
				if (anEnemy != lastEnemy) {
					float anDistance = Vector3.Distance (transform.position, anEnemy.transform.position);
					if (anDistance < bestDistance && anDistance < 10f) {
						bestDistance = anDistance;
						chosenEnemy = anEnemy;
					}
				}
			}
			searchCD = 10;
			if (chosenEnemy) {
				Vector3 anFxScale = BoltFx.transform.localScale;
				anFxScale.y = bestDistance / 5.15f;
				BoltFx.transform.localScale = anFxScale;

				lastPosition = transform.position;
				transform.position = chosenEnemy.transform.position;
				transform.rotation = Quaternion.LookRotation (lastPosition - transform.position);

				chosenEnemy.GetComponent<EnemyLogic> ().ApplyDamage (Damage);
				jumpCount++;
				lastEnemy = chosenEnemy;
			} else {
				Destroy (gameObject);
			}
		}
	}


}
