using UnityEngine;
using System.Collections;

public class BulletBehaviour : MonoBehaviour
{

	public float MoveSpeed = 2f;
	public float Damage;
	public Debuff BulletDebuff;
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	protected virtual void FixedUpdate ()
	{
		transform.position += transform.forward * Time.deltaTime * MoveSpeed;
	}

	protected virtual void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Enemy") {
			other.GetComponent<EnemyLogic> ().ApplyDamage (Damage, BulletDebuff);
			Destroy (gameObject);
		}
	}

	protected void OnTriggerExit (Collider other)
	{
		if (other.name == "BulletContainer") {
			Destroy (gameObject);
		}
	}
}
