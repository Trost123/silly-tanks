using UnityEngine;
using System.Collections;

public class FireBullet : BulletBehaviour
{
	
	// Update is called once per frame
	override protected void FixedUpdate ()
	{
		transform.position += transform.forward * Time.deltaTime * MoveSpeed;
		transform.localScale += new Vector3 (0.07f, 0.07f, 0.07f);
		if (transform.localScale.x > 1.7f) {
			Destroy (gameObject);
		}
	}

	override protected void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Enemy") {
			other.GetComponent<EnemyLogic> ().ApplyDamage (Damage, BulletDebuff);
			//Destroy (gameObject);
		}
	}
}
