﻿using UnityEngine;
using System.Collections;

public class UnitShop : MonoBehaviour
{
    public delegate void SimpleDelegate();
    public static event SimpleDelegate OnMoneyChange = null;
    private static int money;
    public UnityEngine.UI.Text IMoneyText;
    public UnityEngine.UI.Text IHealthText;
    private static UnityEngine.UI.Text MoneyText;
    private static UnityEngine.UI.Text HealthText;
    public GameObject[] Units;
    public GameObject[] Ghosts;
    private GameObject draggedUnit;
    private int tankCount = 0;
    private int lastType = -1;
    private Plane zeroPlane = new Plane(Vector3.up, Vector3.zero);
    private BoxCollider plantHitbox;
    // Use this for initialization
    void Start()
    {
        plantHitbox = GetComponent<BoxCollider>();
        MoneyText = IMoneyText;
        Money = 10000;
    }

    public static int Money
    {
        get
        {
            return money;
        }
        set
        {
            if (value == money)
            {
                Debug.Log("Money is set to same value! UnitShop.cs, line 46");
            }
            money = value;
            MoneyText.text = money.ToString();
            if (OnMoneyChange != null)
            {
                OnMoneyChange();
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (draggedUnit)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            float distance;
            zeroPlane.Raycast(ray, out distance);
            Vector3 hitPoint = ray.GetPoint(distance);
            draggedUnit.transform.position = hitPoint;
        }
    }

    void OnMouseDown()
    {
        if (draggedUnit)
        {
            var spawnedUnit = Instantiate(Units[lastType]);
            spawnedUnit.transform.position = draggedUnit.transform.position;
            spawnedUnit.transform.parent = transform;

            Destroy(draggedUnit);
            draggedUnit = null;
            plantHitbox.enabled = false;
            lastType = -1;
        }
    }

    public void BuyUnit(BuyButtonData buyButtonData)
    {
        if (lastType == -1)
        {
            UnitShop.Money -= buyButtonData.Cost;
            UnitSelection.SwitchToMainCamera();
            lastType = buyButtonData.Id;
            draggedUnit = Instantiate(Ghosts[buyButtonData.Id]);
            draggedUnit.transform.parent = transform;
            plantHitbox.enabled = true;
            if (buyButtonData.Id == 0)
            {
                tankCount++;
                if(tankCount > 4)
                {
                    buyButtonData.DisablePurchases();
                }
            }
        }
    }
}
