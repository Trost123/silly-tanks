﻿using UnityEngine;
using System.Collections;

public class ThreadScroller : MonoBehaviour
{
	public GameObject[] Wheels;
	public int TurnModifier = 1;

	float ForwardSpeed;
	float SideSpeed;
	float threadOffset = 0.25F;
	Renderer Rend;
	Transform tank;
	Rigidbody rbody;
	int counter = 0;
	float threadDirection;
	// Use this for initialization
	void Start ()
	{
		Rend = GetComponent<Renderer> ();
		tank = transform.parent.parent;
		rbody = tank.GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		//float offset = Time.time * scrollSpeed;
		counter++;
		ForwardSpeed = tank.InverseTransformDirection (rbody.velocity).z;
		SideSpeed = rbody.angularVelocity.y;
		if (Mathf.Abs (SideSpeed) > 1) {
			threadDirection = Mathf.Sign (SideSpeed * TurnModifier);
			
			threadOffset -= 0.25f * threadDirection;
			Rend.material.SetTextureOffset ("_MainTex", new Vector2 (0, threadOffset));
			for (int i = 0; i < Wheels.Length; i++) {
				Wheels [i].transform.Rotate (Vector3.right, 6f * threadDirection);
			}
		} else if (counter > 1f / Mathf.Abs (ForwardSpeed)) {
			threadOffset -= 0.25f * Mathf.Sign (ForwardSpeed);
			Rend.material.SetTextureOffset ("_MainTex", new Vector2 (0, threadOffset));
			for (int i = 0; i < Wheels.Length; i++) {
				Wheels [i].transform.Rotate (Vector3.right, 6f * ForwardSpeed);
			}
			counter = 0;
		}
	}
}
