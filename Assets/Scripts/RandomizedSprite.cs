﻿using UnityEngine;
using System.Collections;

public class RandomizedSprite : MonoBehaviour
{
	public Sprite[] Sprites;
	SpriteRenderer sr;
	// Use this for initialization
	void Start ()
	{
		sr = GetComponent<SpriteRenderer> ();
		sr.sprite = Sprites [(int)(Random.value * Sprites.Length)];
	}
}
