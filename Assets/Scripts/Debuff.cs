	
public class Debuff
{
	public DebuffName Name;
	public float Amount;
	public float Duration;
	//static string[] debuffNames = new string[]{"None", "Freeze", "Fire"};
	public enum DebuffName: int
	{
		None,
		Freeze,
		Burning,
		CritOnFrozen,
		CritOnBurning,
		Crit,
		Heal,
		BurnDamage
	}

	public Debuff (DebuffName name, float amount = 0f, float duration = 0f)
	{
		Name = name;
		Amount = amount;
		Duration = duration;
	}
}

