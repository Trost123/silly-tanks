﻿using UnityEngine;
using System.Collections;

public class AnimateSprite : MonoBehaviour
{
	public Sprite[] Sprites;
	public int FrameTime = 20;
	int delay;
	int spriteID;
	SpriteRenderer sr;
	// Use this for initialization
	void Start ()
	{
		delay = FrameTime;
		sr = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		delay--;
		if (delay == 0) {
			spriteID++;
			if (spriteID > Sprites.Length - 1) {
				spriteID = 0;
			}
			sr.sprite = Sprites [spriteID];

			delay = FrameTime;
		}
	}
}
